package com.rahul.mvpdemoapp;

import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoviesInteractorImpl extends AbstractInteractor {

    public MoviesInteractor.CallBack mCallBack;
    public MovieApiInterface movieApiInterface;


    public MoviesInteractorImpl(Executor mThreadExecutor, MainThread mainThread, MoviesInteractor.CallBack callBack) {
        super(mThreadExecutor, mainThread);
        mCallBack=callBack;
    }

    @Override
    public void run() {

        movieApiInterface = ApiClient.getApiClient().create(MovieApiInterface.class);

        Call<List<Movie>> call = movieApiInterface.getAllMovies();

        call.enqueue(new Callback<List<Movie>>() {
            @Override
            public void onResponse(Call<List<Movie>> call, Response<List<Movie>> response) {
                try {
                    mCallBack.onMovieProductDownload(response.body());
                } catch (Exception e) {
                    Log.e("Exception", e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<Movie>> call, Throwable t) {
                mCallBack.onMovieProductDownloadError();
            }
        });

    }
}
