package com.rahul.mvpdemoapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder> {

    Context context;
    List<Movie> movieList;

    public MovieAdapter(Context context, List<Movie> movieList) {
        this.context = context;
        this.movieList = movieList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.layout_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, @SuppressLint("RecyclerView") final int position) {

            //myViewHolder.image.setImageResource(movieList.get(position).getImageurl());
            myViewHolder.txt_name.setText(movieList.get(position).getName());
            myViewHolder.txt_team.setText(movieList.get(position).getTeam());
            //myViewHolder.txt_createdby.setText(movieList.get(i).getCreatedby());

            //boolean isExpanded=movieList.get(position).isExpanded();
            //myViewHolder.expandableLayout.setVisibility(isExpanded ? View.VISIBLE : View.GONE);

    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView txt_name, txt_team, txt_createdby;
        ConstraintLayout expandableLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            txt_name = itemView.findViewById(R.id.txt_name);
            txt_team = itemView.findViewById(R.id.txt_team);
            expandableLayout=itemView.findViewById(R.id.expandableLayout);

            txt_name.setOnClickListener(v -> {
                Movie movie=movieList.get(getAdapterPosition());
                movie.setExpanded(!movie.isExpanded());
                notifyItemChanged(getAdapterPosition());
            });


        }
    }
}
