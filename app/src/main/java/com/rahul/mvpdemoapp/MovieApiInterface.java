package com.rahul.mvpdemoapp;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MovieApiInterface {
    @GET("marvel")
    Call<List<Movie>> getAllMovies();
}
