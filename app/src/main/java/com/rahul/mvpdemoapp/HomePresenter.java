package com.rahul.mvpdemoapp;

import java.util.List;

public class HomePresenter extends AbstractPresenter implements MoviesInteractor.CallBack {

    private HomeView homeView;

    public HomePresenter(Executor executor, MainThread mainThread, HomeView homeView) {
        super(executor, mainThread);
        this.homeView = homeView;
    }

    public void getMoviesList() {
        new MoviesInteractorImpl(mExecutor, mMainThread, this).execute();
    }

    @Override
    public void onMovieProductDownload(List<Movie> movie) {
            if(homeView!=null) {
                homeView.setMarvelHeroes(movie);
            }
    }

    @Override
    public void onMovieProductDownloadError() {

    }
}
