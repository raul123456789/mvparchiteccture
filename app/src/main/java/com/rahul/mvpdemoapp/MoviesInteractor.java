package com.rahul.mvpdemoapp;

import java.util.List;

public interface MoviesInteractor {

    interface CallBack {

        void onMovieProductDownload(List<Movie> movie);

        void onMovieProductDownloadError();
    }

}
